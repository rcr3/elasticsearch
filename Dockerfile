FROM python:3.5-alpine

WORKDIR /srv

LABEL mainteiner="Luiz Antonio <luizgj@iprianga.com.br>"

ADD ./requirements.txt /srv/requirements.txt
ADD ./*.py ./
ADD ./templates/ ./templates/
ADD ./static/ ./static/
ADD ./logging.yaml ./logging.yaml

RUN mkdir /srv/logs/
RUN pip install -r /srv/requirements.txt

ENTRYPOINT ["sh", "-c", "gunicorn -w 4 -b 0.0.0.0:8082 app:app"]
