from random import randrange

errors = ['Erro Bizarro!!!!', 'Não conseguiu salvar ao banco', 'Voto não computado de bobeira',
          'De pau', 'Erro ao recuperar dados', 'Erro aleatório']


def get_one():
    index = randrange(0, len(errors)-1)
    return errors[index]


if __name__ == '__main__':
    print(get_one())