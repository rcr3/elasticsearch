from flask import Flask, render_template, session, request
import logging, app_log
import characters
from flask_restful import Resource, request, reqparse, Api
from werkzeug.wrappers import Response
import json
import os
import exception
import names
import random
import copy


service_host = os.environ['SERVICE_HOST']

app = Flask(__name__)
api = Api(app)
app.config['PROPAGATE_EXCEPTIONS'] = True
app.config['JSON_AS_ASCII'] = False
app_log.setup_logging()
probabilidade = 0.7
logger = logging.getLogger(__name__)


@app.errorhandler(Exception)
def default_error_handler(error):
    status_code = 500
    logger.info('username=%s pesonagem=%s status_code=%s payload=%s' % (request.get_json()['username'],
                                                                        request.get_json()['personagem'],
                                                                        status_code,
                                                                        str(error).replace('\n', ' ')))
    response = Response(json.dumps({'codigo': -100,'mensagem': 'Erro interno. Entrar em contato com a Ipiranga.'}, ensure_ascii=False))
    response.status_code = status_code
    response.headers = {'Content-Type': 'application/json'}
    return response


class Personagem(Resource):

    def get(self):
        status_code = 200
        nome = names.get_full_name()
        personagens = copy.deepcopy(characters.personagens)
        random.shuffle(personagens)
        response = {'username': nome,
                    'personagens': personagens}
        logger.info('username=%s status_code=%s payload=%s' % (nome, status_code, response))
        return response, status_code

    def post(self):
        if(random.random() < probabilidade):
            status_code=200
            logger.info('username=%s pesonagem=%s empresa=%s status_code=%s' %
                        (request.get_json()['username'],
                         request.get_json()['personagem'],
                         request.get_json()['empresa'],
                         status_code))
            return characters.personagens, status_code
        else:
            raise Exception(exception.get_one())


api.add_resource(Personagem, '/personagem')


@app.route('/', methods=['GET'])
def home():
    status_code = 200
    logger.info('status_code=%s' % status_code)
    return render_template('home.html', service_host=service_host)


app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'
