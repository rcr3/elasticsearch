var app = new Vue({
    el: '#app',
    data: {
        personagens: [],
        nome: '',
        username: '',
        sobrenome: '',
        active: false,
        comSucesso: false,
        comErro: false,
        semUser: true
    },
    methods: {
        setUser:  function () {
            this.username = this.nome + ' ' + this.sobrenome;
            this.semUser = false;
        },
        buscarPersonagens: function (service_host) {
            axios
                .get(service_host + "/personagem")
                .then(response => {
                    var data = response.data;
                    this.personagens = data.personagens;
                    }
                )
                .catch(error =>{
                    this.comErro = true;
                })
        },
        selectionarPersonagem: function (personagem, service_host) {
            var data = {
                "username": this.username,
                "personagem": personagem.nome,
                "empresa": personagem.empresa
            }

            axios
                .post(service_host + "/personagem", data)
                .then(response => {
                    this.comSucesso = true;
                    setTimeout(result => {this.comSucesso = false; this.comErro=false} , 2000);
                    }
                )
                .catch(error =>{
                    this.comErro = true;
                    this.comSucesso = false;
                    setTimeout(result => {this.comSucesso = false; this.comErro=false} , 2000);
                })
        }
    },
    beforeMount() {
        this.buscarPersonagens(service_host=service_host);
    }
});
