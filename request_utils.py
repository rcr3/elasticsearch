# ! /usr/bin/python
import uuid
import logging
import flask


def has_numbers(inputString):
    return any(char.isdigit() for char in inputString)


def request_user_agent():
    for header in flask.request.headers:
        if 'User-Agent' in header[0]:
            user_agent = header[1]
    return user_agent


def request_path():
    path = flask.request.__dict__['environ']['PATH_INFO']
    resource = new_path = ''
    for splited in path.split('/') :
        if splited != '':
            if has_numbers(splited):
                resource = '/{resourceId}'
            else:
                resource = '/'+splited
        new_path += resource
    return new_path


def request_method():
    method = flask.request.__dict__['environ']['REQUEST_METHOD']
    return method


def request_ip():
    ip = flask.request.__dict__['environ']['REMOTE_ADDR']
    return ip


def request_query():
    query = flask.request.__dict__['environ']['QUERY_STRING']
    if query:
        query = {x[0]: x[1] for x in [x.split("=") for x in query.split("&")]}
        if query.__contains__('token'):
            query['token'] = '**********'
        return query
    else:
        return None


class RequestFilter(logging.Formatter):

    def __init__(self):
        super().__init__(fmt="%(asctime)s %(name)s.%(module)s.%(funcName)s=%(lineno)d %(levelname)s - user_agent=%(request_user_agent)s metodo=%(request_method)s recurso=%(request_path)s ip=%(request_ip)s query=%(request_query)s %(message)s")

    def format(self, record):
        """
        Adicionar as informações basicas necessarias para o logging da API.
        """

        record.request_user_agent = request_user_agent() if flask.has_request_context() else ''
        record.request_path = request_path() if flask.has_request_context() else ''
        record.request_method = request_method() if flask.has_request_context() else ''
        record.request_query = request_query() if flask.has_request_context() else ''
        record.request_ip = request_ip() if flask.has_request_context() else ''
        return super().format(record)