
# README #

Este repositorio possui dados para apresentação do ELK da elastic 
https://www.elastic.co

### Qual o conteúdo ###

* Elasticsearch
* Aplicação para votação de super heróis

### Como utilizar ###

### Elasticsearch/APP

Baixar e instalar [docker](https://docs.docker.com/install/)
	
Baixar e instalar [docker-compose](https://docs.docker.com/compose/install/)
	
Executar o arquivo docker-compose

```bash
docker-compose up
```

Acessar o [link](localhost:8082)

Elasticsearch [link](localhost:9200)

Elasticsearch [link](localhost:5601)

### Com que falar ###
Luiz Antonio Guimaraes Junior
* luizgj@ipiranga.com.br